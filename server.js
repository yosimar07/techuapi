//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser')
app.use(bodyparser.json())
app.use(function(req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

var requestjson = require('request-json');

var urlGas = "https://api.datos.gob.mx/v1/precio.gasolina.publico"
var gas = requestjson.createClient(urlGas);

var urlClientesMLab = "https://api.mlab.com/api/1/databases/yarellano/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientesMLab);

var urlUsuariosMLab = "https://api.mlab.com/api/1/databases/yarellano/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuariosMLab = requestjson.createClient(urlUsuariosMLab);

var urlMovimientosMLab = "https://api.mlab.com/api/1/databases/yarellano/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var movimientosMLab = requestjson.createClient(urlMovimientosMLab);

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/yarellano/collections";

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req, res) {
  //res.send('Hemos recibido su petición get');
  res.sendFile(path.join(__dirname,'index.html'))
})

app.post('/', function (req, res) {
  res.send('Hemos recibido su petición post')
})

app.put('/', function (req, res) {
  res.send('Hemos recibido su petición put')
})

app.delete('/', function (req, res) {
  res.send('Hemos recibido su petición delete')
})

app.get('/v1/Clientes/:idCliente', function(req,res){
  res.send('Aqui tiene al cliente número: ' + req.params.idCliente);

})

// Alta usuarios
app.post('/v3/Clientes',function(req,res){

 usuariosMLab.post('', req.body, function(err, resM, body){
   res.send(body);
 })

 var nombre = req.body.nombre
 var apellido = req.body.apellido
 var email = req.body.email
 var password = req.body.password
 var query = 'q={"email": "'+ password +'", "password": "'+ password +'", "nombre": "'+ nombre +'", "apellido": "'+ apellido +'"}';


 console.log("Variables:"+query);


 clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&"+ query);
 console.log ("URL:"+ clienteMLabRaiz);

 clienteMLabRaiz.post('',function(err, resM, body){
   if (!err){
     if (body.length == 1)
     {
       res.status(200).send('Usuario dado de alta' );
     }
     else{
       res.status(404).send('error');
     }
   }else {
     console.log(body);
   }

 });
})


//Login
app.post('/v4/login',function (req,res) {
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';
  usuariosMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);

  console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);


  clienteMLabRaiz.get('',function (err,resM,body) {
    if (!err) {
      if (body.length==1) { //login ok
        res.status(200).send('Usuario logeado');
      }else {
        res.status(404).send('Usuario no encontrado. Registrese.');
      }
    }else {
      console.log(body);
    }
  })
})

//Alta de movimientos

app.post('/v4/Movimientos',function(req,res){


 movimientosMLab.post('', req.body, function(err, resM, body){
   res.send(body);
 })

 var cuenta = req.body.cuenta
 var cliente = req.body.cliente
 var fecha = req.body.fecha
 var importe = req.body.importe
 var tipo = req.body.tipo

 var query = 'q={"cuenta": "'+ cuenta +'", "cliente": "'+ cliente +'", "fecha": "'+ fecha +'", "importe": "'+ importe +'", "tipo": "'+ tipo +'"}';


 console.log("Variables:"+query);


 clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Movimientos?" + apiKey + "&"+ query);
 console.log ("URL: "+ urlMLabRaiz + "/Movimientos?" + apiKey + "&"+ query);

 clienteMLabRaiz.post('',function(err, resM, body){
   if (!err){
     if (body.length == 1)
     {
       res.status(200).send('Usuario dado de alta' );
     }
     else{
       res.status(404).send('error');
     }
   }else {
     console.log(body);
   }

 });
})

//Consulta movimientosV2JSON
app.get('/v4/Movimientos', function(req,res){


  movimientosMLab.get('',function(err,resM,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }

  })
})

//Consulta gas
app.get('/v1/precio.gasolina.publico', function(req,res){


  gas.get('',function(err,resM,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }

  })
})
